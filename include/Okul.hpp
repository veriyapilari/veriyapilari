/*     
 * @file		Okul.hpp
 * @description	Heap bellek bölgesinde dizi işlemleri
 * @course		Veri Yapıları
 * @assignment 	1. Ödev
 * @date		14.10.2018
 * @author		Muhammet ÖMER g171210558@sakarya.edu.tr
 */

#ifndef OKUL_HPP
#define OKUL_HPP

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Okul{
	private:	
		char harf;
		string heap_Adres;
		
		void AdresAta();
	public:
		Sepet(char);
		char Harf();
		string Adres();
		friend ostream& operator<<(ostream&,Sepet*&);
};
#endif